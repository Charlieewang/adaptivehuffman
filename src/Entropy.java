import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Entropy 
{
	File file;
	int[] count;
	byte[] inputFile;
	
	public Entropy(String fileName) throws IOException
	{
		count = new int[256];
		file = new File(fileName);
		inputFile = Files.readAllBytes(file.toPath());
	}
	
	public void count()
	{
		int temp = 0;
		
		for(int i = 0; i < file.length(); i++)
		{
			temp = ((Byte)inputFile[i]).intValue() & 0xff;
			count[temp] ++;
		}
	}
	
	public float entropy()
	{
		count();
		
		float entropy = 0;
		for(int i = 0; i < count.length; i++)
		{
			float p = (float)count[i] / file.length();
			if( p > 0)
			{
				entropy = entropy - (p * Log2(p));
			}
		}
		
		return entropy;
	}
	
	private float Log2(float n) 
	{
	    return (float) (Math.log(n) / Math.log(2));
	}
}
