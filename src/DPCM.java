import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class DPCM
{
	int[][] intArray = null, output = null;
	byte[] origin = null;
	private FileOutputStream out = null;
	int length = 512, width = 512;

	public static void main(String arg[]) throws IOException
	{
		File input = new File("baboon_dpcm_decode");
		byte[] inputFile = Files.readAllBytes(input.toPath());
		DPCM dpcm = new DPCM(inputFile);

		int[] dpcmOutput = dpcm.deprocessing();
		dpcm.output(dpcmOutput);


	}

	public DPCM(byte[] origin)
	{
		intArray = new int[length][width];
		output = new int[length][width];
		this.origin = origin;

		try
		{
			out = new FileOutputStream("baboon_dpcm_decode_recover.raw");
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		//read original image and output a processed file
		//read a decoded dpcm image, deprocessing and output image
	}

	public void init()
	{
		intArray = new int[length][width];
		output = new int[length][width];
	}

	public int[] processing()
	{
		init();
		assign();
		minus();
		
		return combineTo1D();
	}
	
	public int[] deprocessing()
	{
		init();
		assign();
		add();
		
		return combineTo1D();
	}
	
	public void assign()
	{

		int x = 0;
		
		for(int i = 0; i < length; i++)
		{
			for(int j = 0; j < width; j++)
			{
				intArray[i][j] = ((Byte)origin[x]).intValue() & 0xff;
				x = x + 1;
			}
		}
	}
	
	public void minus()
	{
		output[0][0] = intArray[0][0] - 128;
		
		//horizon
		for(int i = 0; i < length; i++)
		{
			for(int j = 1; j < width; j++)
			{// minus left
				output[i][j] = intArray[i][j] - intArray[i][j-1];
			}
		}
		
		for(int i = 1; i < length; i++)
		{// minus up
			output[i][0] = intArray[i][0] - intArray[i - 1][0];
			
		}
	}
	
	public void add()
	{
		output[0][0] = intArray[0][0] + 128;

		for(int i = 1; i < length; i++)
		{// minus up
			output[i][0] = intArray[i][0] + output[i - 1][0] & 0xff;
		}

		//horizon
		for(int i = 0; i < length; i++)
		{
			for(int j = 1; j < width; j++)
			{// minus left
				output[i][j] = intArray[i][j] + output[i][j-1];
			}
		}
	}
	
	public int[] combineTo1D()
	{
		int[] array = new int[length*width];
		int x = 0;

		for(int i = 0; i < length; i++)
		{
			for(int j = 0; j < width; j++)
			{
				array[x] = output[i][j];
				x = x + 1;
			}
		}
		
		return array;
	}

	public void output(int[] array)
	{
		int x = 0, temp = 0;

		try
		{
			for (int i = 0; i < length; i++)
			{
				for (int j = 0; j < width; j++)
				{
					out.write(array[x] & 0xff);
//					System.out.println(array[x] % 256);
					x = x + 1;
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
