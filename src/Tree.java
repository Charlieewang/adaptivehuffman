import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Tree 
{
	private Node NYTPointer, root;
	private Map<Integer, Node> nodeMap; // (key, value)
	private ArrayList<Node> nodeArray; // for searching
	private static int INDEX_START = 511;
//	String NYT = new String();

	//TODO how to arrange node index ?
	
	public Tree()
	{
		root = new Node(INDEX_START);
		root.setNYT(true);
		NYTPointer = root;
		nodeMap = new HashMap<Integer, Node>();
		nodeArray = new ArrayList<Node>();
	}
	
	public Node getRoot()
	{
		return root;
	}
	
	public boolean isEmpty()
	{
		return NYTPointer.equals(root);
	}
	
	public String getNYTPath()
	{
		return codePath(NYTPointer);
	}
	
	public boolean firstRead(Node node) 
	{
		// first run
		if(NYTPointer.equals(root))
		{
			return true;
		}
		else
		{
			// check if node is already exist
			if(nodeMap.containsKey(node.getPixelValue()))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	
	public boolean firstRead(int value) 
	{
		// first run
		if(NYTPointer.equals(root))
		{
			return true;
		}
		else
		{
			// check if node is already exist
			if(nodeMap.containsKey(value))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	public void updateTree(Node node) 
	{
		Node target;
		
		// find the exist node
		target = nodeMap.get(node.getPixelValue());
		checkBlock(target);
	}

	public void addNewNode(Node node) //NYT give birth
	{
		// generate NYT node (left) and leaf (right)
		Node newNYT = new Node(NYTPointer.getIndex() - 2);
		newNYT.setNYT(true);

		node.setParent(NYTPointer);
		newNYT.setParent(NYTPointer);
		NYTPointer.setLeftNode(newNYT);
		NYTPointer.setRightNode(node); // new leaf
		nodeArray.add(newNYT);
		nodeArray.add(node);
		
		
		// TODO leaf node setting
		node.setIndex(NYTPointer.getIndex() - 1);
		node.setLeaf(true);
		node.increaseWeight();

		
		// TODO parent node's weight update
		NYTPointer.increaseWeight(); // old NYT increase weight
		
		NYTPointer.setNYT(false);
		checkBlockForNewAdd(NYTPointer);// store old NYT
		
		NYTPointer = newNYT; // move NYT pointer
		
		// add to hash map
		nodeMap.put(node.getPixelValue(), node);
	}

	void checkBlockForNewAdd(Node targetNode)
	{
		Node temp, maxIndex;
		boolean changeFlag = false;
		
		while(!targetNode.equals(root))
		{
			changeFlag = false;
			targetNode = targetNode.getParent();
			maxIndex = targetNode;
			
			// TODO check for block
			for(int i = 0; i < nodeArray.size(); i++)
			{
				temp = nodeArray.get(i);
				if(temp != targetNode.getParent() && temp.getWeight() == targetNode.getWeight())
				{
					if(temp.getIndex() > maxIndex.getIndex())
					{
						maxIndex = temp; // find max index
						changeFlag = true;
					}
				}
			}

			if(changeFlag)
			{
				swap(maxIndex, targetNode);
			}
			
			// increment
			targetNode.increaseWeight();
		}
	}
	
	void checkBlock(Node targetNode)
	{
		Node temp, maxIndex;
		boolean changeFlag = false, isRoot = false;
		
		do
		{
			changeFlag = false;
			maxIndex = targetNode;
			
			// TODO check for block
			for(int i = 0; i < nodeArray.size(); i++)
			{
				temp = nodeArray.get(i);
				if(temp != targetNode.getParent() && temp.getWeight() == targetNode.getWeight())
				{
					if(temp.getIndex() > maxIndex.getIndex())
					{
						maxIndex = temp; // find max index
						changeFlag = true;
					}
				}
				
			}

			if(changeFlag)
			{
				swap(maxIndex, targetNode);
			}
			
			// increment
			targetNode.increaseWeight();
			
			if(targetNode.equals(root))
			{
				isRoot = true;
			}
			else
			{
				targetNode = targetNode.getParent();
			}
			
			
		}while(!isRoot);
	}


	void swap(Node nodeA, Node nodeB)
	{
		// change parent, index
		Node t = new Node();
		Node parentA, parentB;
		parentA = nodeA.getParent();
		parentB = nodeB.getParent();
		
		
		if(parentA.getLeftNode().equals(nodeA))
		{
			parentA.setLeftNode(nodeB);
		}
		else
		{
			parentA.setRightNode(nodeB);
		}
		
		if(parentB.getLeftNode().equals(nodeB))
		{
			parentB.setLeftNode(nodeA);
		}
		else
		{
			parentB.setRightNode(nodeA);
		}
		
		t.setIndex(nodeA.getIndex());
		t.setParent(nodeA.getParent());
		
		nodeA.setParent(nodeB.getParent());
		nodeA.setIndex(nodeB.getIndex());
		nodeB.setParent(t.getParent());
		nodeB.setIndex(t.getIndex());
	}
	
	public String codePath(Node input)
	{
		Node parent, in = input;
		
		//TODO maybe to String?
		String buffer = new String();
		
		
		while(in.getParent() != null)
		{
			parent = in.getParent();
			if(parent.getLeftNode().equals(in))
			{
				buffer = buffer + '0';
			}
			else
			{
				buffer = buffer + '1';
			}
			in = parent;
		}
		
		buffer = new StringBuilder(buffer).reverse().toString();
		
		return buffer;
	}
	
	public String codePath(int value)
	{
		Node parent, in = nodeMap.get(value);
		
		//TODO maybe to String?
		String buffer = new String();
		
		
		while(in.getParent() != null)
		{
			parent = in.getParent();
			if(parent.getLeftNode().equals(in))
			{
//				System.out.print(0);
				buffer = buffer + '0';
			}
			else
			{
//				System.out.print(1);
				buffer = buffer + '1';
			}
			
			in = parent;
		}
		
		buffer = new StringBuilder(buffer).reverse().toString();
		
		return buffer;
	}
	
}
