import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class Encode 
{

	Tree tree;
	BitOutputStream outputStream = null;
	
	public static void main(String arg[]) throws IOException
	{
		Encode encode = new Encode();
		File file = new File("Lena.raw");
		byte[] inputFile = Files.readAllBytes(file.toPath());
		long start = System.currentTimeMillis();
		Entropy entropy = new Entropy("Lena.raw");
		
//		int[] test = {8, 255, 4, 255, 5, 5, 32, 44};
		
//		encode.encoding(inputFile);
//		encode.encodingTest(test);
		
		System.out.println(entropy.entropy());
		
		long end = System.currentTimeMillis();
		long costtime = end - start;
		
		System.out.println("\ncostTime: " + costtime);
		System.out.println("inputBytes: " + file.length());
	}

	public Encode()
	{
		tree = new Tree();
		
		try 
		{
			outputStream = new BitOutputStream(new FileOutputStream("out"));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void encoding(byte[] inputFile)
	{
		int inputByte = 0, total = 0;
		
		for(int i = 0; i < inputFile.length ; i++)
		{
			inputByte = ((Byte)inputFile[i]).intValue() & 0xff;
			total = total + this.output(inputByte);
			this.readNode(new Node(inputByte, false));
		}
		
		try 
		{
			outputStream.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
//		System.out.println(total);
		
	}
	
	public int convertByteToInt(byte[] b)
	{           
	    int value= 0;
	    for(int i=0; i<b.length; i++)
	       value = (value << 8) | b[i];     
	    return value;       
	}
	
	public void encodingTest(int[] inputFile)
	{
		int inputByte, total = 0;
		
		for(int i = 0; i < inputFile.length ; i++)
		{
			inputByte = inputFile[i];
			
			total = total + this.output(inputByte);
			this.readNode(new Node(inputByte, false));
		}
		
		try 
		{
			outputStream.close();
			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public void readNode(Node node)
	{
		if(tree.firstRead(node) == true)
		{
			tree.addNewNode(node);
		}
		else
		{
			tree.updateTree(node);
		}
	}
	
	public String getCode(Node node)
	{
		return tree.codePath(node);
	}
	
	public String getCode(int value)
	{
		return tree.codePath(value);
	}
	
	public int output(int in)
	{
		String huffmanCode = new String();
		
		try 
		{
			if(tree.firstRead(in))
			{
				huffmanCode = tree.getNYTPath();
				huffmanCode = huffmanCode + integerToString(in);
			}
			else
			{
				huffmanCode = getCode(in);
			}

//			System.out.print(huffmanCode);
			
		    for (int i = 0; i < huffmanCode.length(); i++) 
			{
		    	outputStream.write(huffmanCode.charAt(i) == '1');
			}
		    
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		return huffmanCode.length();
	}
	

	String integerToString(int input)
	{
		String temp, out;
		out = new String();
		int newIndex = 0;
		
		temp = Integer.toBinaryString(input); //100
		
		for(int i = 7; i >= 0; i--)
		{
			if(temp.length() > i)
			{
				newIndex = temp.length() - i - 1;
				if(temp.charAt(newIndex) == '1')
				{
					out = out + '1';
				}
				else
				{
					out = out + '0';
				}
			}
			else
			{
				out = out + '0';
			}
		}
		
		return out;
	}
}
