import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Decode 
{
	Tree tree;
	BitInputStream in = null;
	FileOutputStream out = null;
	BitOutputStream outputStream = null;
	
	public static void main(String arg[]) throws IOException
	{
		Decode decode = new Decode("out", "out_decode"); // input, output
		long start = System.currentTimeMillis();
		decode.decoding();
		long end = System.currentTimeMillis();
		long costtime = end - start;
		System.out.println("\ncostTime: " + costtime);
	}
	
	public Decode(String input, String output)
	{
		tree = new Tree();
		
		try 
		{
			in = new BitInputStream(new BufferedInputStream(new FileInputStream(input)));
			out = new FileOutputStream(output);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	public int getFileSize(String filename) throws IOException
	{
		File file = new File(filename);
		return (int) file.length();
	}

	public void decoding() throws IOException
	{
		Node node = tree.getRoot();
		String temp = new String();
		int outInt = 0, size = 0;
		
		if(tree.isEmpty())//read first byte
		{
			for(int i = 0; i < 8; i++)
			{
				if(in.read())
				{
					temp = temp + '1';
				}
				else
				{
					temp = temp + '0';
				}
			}
			
			//output
			outInt = Integer.parseInt(temp, 2);
			out.write(outInt);
			tree.addNewNode(new Node(outInt, false));
		}
		
//		System.out.println(Integer.parseInt(temp, 2));

		int x = 0, buffer = 1;
		size = 512*512;
		while(buffer < size)
		{
			if(in.read())
			{
				node = node.getRightNode();
			}
			else
			{
				node = node.getLeftNode();
			}
			
			if(node.isNYT())
			{
				temp = "";
				for(int i = 0; i < 8; i++)
				{
					if(in.read())
					{
						temp = temp + '1';
					}
					else
					{
						temp = temp + '0';
					}
					
					x = x + 1;
				}
				
				//output
				outInt = Integer.parseInt(temp, 2);
				out.write(outInt);
				tree.addNewNode(new Node(outInt, false));
//				System.out.println("NYT: " + Integer.parseInt(temp, 2));
				node = tree.getRoot();
				buffer ++;
			}
			else if(node.isLeaf())
			{
				tree.updateTree(node);
				out.write(node.getPixelValue());
//				System.out.println("Leaf: " + node.getPixelValue());
				node = tree.getRoot();
				buffer ++;
			}
			
			x = x + 1;
		}
		
		out.close();
		
	}
}