import java.io.FileOutputStream;

public class Node
{
	private boolean isNYT, isLeaf;
	private int weight = 0, index = -1;
	private int pixelValue;
	private Node leftNode, rightNode, parent;
	
	//TODO 每個(除了leaf) node 的 weight 應該是全部子 node 的總和

	public Node(int index)
	{
		this.index = index;
		isNYT = false;
		isLeaf = false;
		weight = 0;
		index = -1;
		leftNode = null;
		rightNode = null;
		leftNode = null;
		rightNode = null;
		parent = null;
	}
	
	public Node(int value, boolean flag)
	{
		this.pixelValue = value;
		isNYT = false;
		isLeaf = false;
		weight = 0;
		index = -1;
		leftNode = null;
		rightNode = null;
		leftNode = null;
		rightNode = null;
		parent = null;
	}

	public Node()
	{
		isNYT = false;
		isLeaf = false;
		weight = 0;
		index = -1;
		leftNode = null;
		rightNode = null;
		leftNode = null;
		rightNode = null;
		parent = null;
	}
	

	public boolean isNYT() 
	{
		return isNYT;
	}

	public void setNYT(boolean isNYT) 
	{
		this.isNYT = isNYT;
	}

	public int getWeight() 
	{
		return weight;
	}

	public void setWeight(int weight) 
	{
		this.weight = weight;
	}

	public void increaseWeight()
	{
		weight = weight + 1;
	}

	public int getIndex() 
	{
		return index;
	}

	public void setIndex(int index) 
	{
		this.index = index;
	}

	public int getPixelValue() 
	{
		return pixelValue;
	}

	public void setPixelValue(int pixelValue) 
	{
		this.pixelValue = pixelValue;
	}


	public boolean isLeaf() 
	{
		return isLeaf;
	}

	public void setLeaf(boolean leaf) 
	{
		isLeaf = leaf;
	}

	public Node getLeftNode() 
	{
		return leftNode;
	}

	public void setLeftNode(Node leftNode) 
	{
		this.leftNode = leftNode;
	}

	public Node getRightNode() 
	{
		return rightNode;
	}

	public void setRightNode(Node rightNode)
	{
		this.rightNode = rightNode;
	}

	public Node getParent() 
	{
		return parent;
	}

	public void setParent(Node parent) 
	{
		this.parent = parent;
	}

	public String toString()
	{
		return "\nindex: " + this.getIndex()
				+ "\nvalue: " + this.getPixelValue()
				+ "\nisNYT: " + this.isNYT()
				+ "\nisLeaf: " + this.isLeaf()
				+ "\nweight: " + this.getWeight();
	}
}
